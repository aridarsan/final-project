import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as GLOBAL from '../constant';
import DateTimePicker from '@react-native-community/datetimepicker';
 
export default function BookPage({navigation, route}) {
  const {item} = route.params;
  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [duration, setDuration] = useState(1000);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  // LOCAL TIME
  const offsetDate =  date.getTimezoneOffset() * 60 * 1000;
  const localDate = new Date(date.getTime() - offsetDate);

  // START TIME
  const offsetStartTime = time.getTimezoneOffset() * 60 * 1000;
  const startTime = new Date(time.getTime() - offsetStartTime);
  console.log('start', startTime)
  
  // END TIME
  const convertDuration = duration/1000
  const endTime = new Date(time.getTime() + 25200000 + ( convertDuration*3600000));
  console.log('endtime', endTime)

  const convertDateToString = (selectedDate) => { 
    const currDate = selectedDate.getDate(); 
    const currMonth = selectedDate.getMonth() + 1;
    const currYear = selectedDate.getFullYear(); 
    return  ' ' + currDate + ' ' + '-' + ' ' + currMonth + ' ' + '-' + ' ' + currYear 
  };

  const convertTimeToString = (selectedTime) => { 
    const currHour = ('0' + selectedTime.getHours()).slice(-2)
    const currMinute = ('0' + selectedTime.getMinutes()).slice(-2)
    return currHour + ':' + currMinute
  }  

   const onChange = (event, selectedValue) => {
      setShow(Platform.OS === 'ios');
      if (mode == 'date') {
        const currentDate = selectedValue || new Date();
        setDate(currentDate);
        setMode('time');
        setShow(Platform.OS !== 'ios');
      } else {
        const selectedTime = selectedValue || new Date();
        setTime(selectedTime);
        setShow(Platform.OS === 'ios');
        setMode('date');
      }
    };
    const showMode = currentMode => {
      setShow(true);
      setMode(currentMode);
    };

    const showDatepicker = () => {
      showMode('date');
    };

    const Rupiah = (price) => {
      let priceString = `${price}`,
        rest = priceString.length % 3,
        rp = priceString.substr(0, rest),
        thousand = priceString.substr(rest).match(/\d{3}/g);

      if (thousand) {
        let separator = rest ? '.' : '';
        rp += separator + thousand.join('.');
      }
      return rp;
    };

  return (
    <View style={GLOBAL.CONTAINER}>
      <View style={styles.content}>
        <Text style={GLOBAL.BOLDBLUEFONT}>{item.name}</Text>
        <Text style={GLOBAL.PRICECONT}>Rp {Rupiah(item.price *duration)}</Text>
      </View>
      <View>
        <View style={styles.margin}>
          <Text style={GLOBAL.LABELFONT}>Field</Text>
          <Text style={GLOBAL.CONTENTFONT}>{item.name}</Text>
        </View>
        <View style={styles.margin}>
          <Text style={GLOBAL.LABELFONT}>Field Address</Text>
          <Text style={GLOBAL.CONTENTFONT}>{item.address}</Text>
        </View>
        <View style={styles.margin}>
            <Text style={GLOBAL.LABELFONT}>Schedule</Text>
          <View style={styles.dateTimeStyle}>
            <Text style={GLOBAL.DATEFONT}>{date ? convertDateToString(date) : 'Please =>'}</Text>
            <Text style={GLOBAL.DATEFONT}>{time ? convertTimeToString(time) : 'Please =>'}</Text>
            <TouchableOpacity 
              onPress={showDatepicker}>
              <Text style={GLOBAL.SELECTFONT}>Pick Schedule</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.margin}>
            <Text style={GLOBAL.LABELFONT}>Duration</Text>
          <View style={styles.dateTimeStyle}>
            <TouchableOpacity 
              style={GLOBAL.DURATIONBTN}
              onPress={() => setDuration(1000)}>
              <Text style={GLOBAL.SELECTFONT}>1</Text>
            </TouchableOpacity>
            <TouchableOpacity 
              style={GLOBAL.DURATIONBTN}
              onPress={() => setDuration(2000)}>
              <Text style={GLOBAL.SELECTFONT}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity 
              style={GLOBAL.DURATIONBTN}
              onPress={() => setDuration(3000)}>
              <Text style={GLOBAL.SELECTFONT}>3</Text>
            </TouchableOpacity>
            <TouchableOpacity 
              style={GLOBAL.DURATIONBTN}
              onPress={() => setDuration(4000)}>
              <Text style={GLOBAL.SELECTFONT}>4</Text>
            </TouchableOpacity>
          </View>
        </View>
        

        <View>
         {show && (
        <DateTimePicker
              testID='dateTimePicker'
              timeZoneOffsetInMinutes={0}
              value={date}
              mode={mode}
              is24Hour={true}
              display='default'
              onChange={onChange}
            />
          )}
        </View>
      </View>
      <View style={styles.bookButton}>
        <TouchableOpacity onPress={() => book()}>
          <View style={GLOBAL.MAINBUTTON}>
            <Text style={GLOBAL.TXTMAINBUTTON}>Check Availability </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#222',
    height: GLOBAL.DIMENSHEIGHT
  },
  content: {
    marginTop: 50,
    marginBottom: 50,
  },
  margin: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  bookButton: {
    position:'absolute',
    bottom:100
  },
  dateTimeStyle : {
    marginVertical: 8,
    flexDirection:'row',
    display:'flex',
    justifyContent: 'space-between'
  },
});
