import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import BookingPage from "./components/BookingPage";
import BrowseField from "./components/BrowseField";
import FieldDetail from "./components/FieldDetail";
// import FireBase from "./components/firebase";
import Footer from "./components/Footer";
import Home from "./components/Home";
import NavBar from "./components/NavBar";
import Profile from "./components/Profile";
// import firebase from "firebase/app";
// import "firebase/auth";

const App = () => {
  const [isLogin, setIsLogin] = useState(false);

  return (
    <>
      <NavBar isLogin={isLogin} setIsLogin={setIsLogin} />
      <Switch>
        <Route exact path="/profil">
          <Profile />
        </Route>

        <Route exact path="/browse-field">
          <BrowseField />
        </Route>

        <Route exact path="/">
          <Home/>
        </Route>

        <Route exact path="/booking-page/:id">
          <BookingPage isLogin={isLogin} setIsLogin={setIsLogin}/>
        </Route>
{/* 
        <Route exact path="/firebase">
          <FireBase/>
        </Route> */}

        <Route exact path="/field-detail/:id">
          <FieldDetail isLogin={isLogin} setIsLogin={setIsLogin}/>
        </Route>
      </Switch>
      <Footer />
    </>
  );
};

export default App;
