import React, { useState } from "react";
import { Link } from "react-router-dom";
import ModalAfterBooking from "./ModalAfterBooking";
import axios from "axios";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Container,
} from "reactstrap";

const ModalBooking = (props) => {
  const [modalBook, setModalBook] = useState(false);
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const { className } = props;
  const toggleBook = () => setModalBook(!modalBook);

  const handleBook = (e) => {
    e.preventDefault();

    axios
    .post()
    .then((res) => {
      // setModalBook(false);
      console.log(props);
    });
  };

  return (
    <div>
      <Link
        onClick={toggleBook}
        style={{
          textDecoration: "none",
        }}
      >
        <Button className="col-12 mt-3 btn-book">Book Now</Button>
      </Link>

      <Modal
        isOpen={modalBook}
        toggle={toggleBook}
        className={className}
        id="modal-book"
      >
        <ModalBody>
          <h3 className="title">Field A</h3>
          <Form onSubmit={handleBook}>
            <FormGroup>
              <Label for="date">Date</Label>
              <Input
                type="date"
                name="date"
                id="date"
                className="col-12 m-auto"
                onChange={(e) => setDate(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="Time">Time</Label>
              <Container>
                <Row onChange={(e) => setTime(e.target.value)}>
                  <h4 className="col-3">14.00</h4>
                  <h4 className="col-3">15.00</h4>
                  <h4 className="col-3">16.00</h4>
                  <h4 className="col-3">17.00</h4>
                  <h4 className="col-3">14.00</h4>
                  <h4 className="col-3">15.00</h4>
                  <h4 className="col-3">16.00</h4>
                  <h4 className="col-3">17.00</h4>
                  <h4 className="col-3">14.00</h4>
                  <h4 className="col-3">15.00</h4>
                  <h4 className="col-3">16.00</h4>
                  <h4 className="col-3">17.00</h4>
                  <h4 className="col-3">14.00</h4>
                  <h4 className="col-3">15.00</h4>
                  <h4 className="col-3">16.00</h4>
                  <h4 className="col-3">17.00</h4>
                </Row>
              </Container>
            </FormGroup>

            <FormGroup>
              <h4>Review Your Book</h4>
              <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                Date : {date}
              </h5>
              <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                Time : {time}
              </h5>
            </FormGroup>
            <FormGroup>
              <ModalAfterBooking modal={toggleBook} date={date} time={time} />
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalBooking;
