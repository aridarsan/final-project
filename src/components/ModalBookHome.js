import React, { useState } from "react";
// import ModalAfterBooking from "./ModalAfterBooking";
import axios from "axios";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  // Row,
  // Container,
} from "reactstrap";
// import { useParams } from "react-router-dom";

const ModalBook = (props) => {
  //Modal
  const [modalBook, setModalBook] = useState(false);
  const [modalAfterBooking, setModalAfterBooking] = useState(false);
  const [closeAll, setCloseAll] = useState(false);

  //input
  const [date, setDate] = useState("");
  const [start, setStart] = useState("");
  const [end, setEnd] = useState("");

  //Submit
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");

  const bookStart = new Date(date + " " + start).toJSON();
  const bookEnd = new Date(date + " " + end).toJSON();

  // const [bookingInput, setBookingInput] = useState(null)
  const { className } = props;
  const toggle = () => setModalBook(!modalBook);

  const toggleNested = () => {
    setModalAfterBooking(!modalAfterBooking);
    setCloseAll(false);
  };
  const toggleAll = () => {
    setModalAfterBooking(!modalAfterBooking);
    setCloseAll(true);
  };

  // const params = useParams();

  const baseUrl = "http://ec2-3-0-98-35.ap-southeast-1.compute.amazonaws.com";

  const token = localStorage.getItem("token");

  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  // useEffect(() => {

  //   console.log(id)

  //   axios
  //     .get(`${baseUrl}/booking?id=${id}`)
  //     .then((res) => {
  //       // console.log(res.data);
  //       setBookingInput(res.data);
  //     })
  //     .catch((err) => console.log(err));
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  // console.log(bookingInput)

  const handleBook = (e) => {
    e.preventDefault();
    console.log(bookStart);
    console.log(bookEnd);
    const data = {
      startTime: startTime,
      endTime: endTime,
    };

    axios
      .post(`${baseUrl}/booking/${props.id}`, data, config)
      .then((res) => {
        console.log(res)
        // setModalBook(false);
        console.log(data);
      })
      .catch((err) => console.log(err));
  };

  return (
    //Modal Utama
    <div>
      <Button
        onClick={toggle}
        className="col-12 btn-book"
        style={{ width: "88.750px" }}
      >
        Book
      </Button>

      <Modal
        isOpen={modalBook}
        toggle={toggle}
        className={className}
        id="modal-book"
      >
        <ModalBody>
          {/* Modal Pertama */}
          <h3 className="title">{props.name}</h3>
          <Form>
            <FormGroup>
              <Label for="startTime">Date</Label>
              <Input
                type="date"
                name="startTime"
                id="startTime"
                className="col-12 m-auto"
                onChange={(e) => setDate(e.target.value)}
              />
            </FormGroup>

            <Row style={{ padding: "0 18px" }}>
              <FormGroup>
                <Label for="endTime">Start Time</Label>
                <Input
                  type="time"
                  name="endTime"
                  id="endTime"
                  className="col"
                  onChange={(e) => setStart(e.target.value)}
                />
              </FormGroup>

              <FormGroup className="ml-auto">
                <Label for="endTime">End Time</Label>
                <Input
                  type="time"
                  name="endTime"
                  id="endTime"
                  className="col"
                  onChange={(e) => setEnd(e.target.value)}
                />
              </FormGroup>
            </Row>

            <Form onSubmit={handleBook}>
              <h4>Review Your Book</h4>
              <FormGroup>
                <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                  Start Time :{" "}
                </h5>
                <Input
                  type="text"
                  name="startTime"
                  id="startTime"
                  onChange={(e) => setStartTime(e.target.value)}
                  style={{ textAlign: "center" }}
                  placeholder={bookStart}
                />
                {/* {date}T{startTime}:00Z */}
              </FormGroup>

              <FormGroup>
                <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                  End Time :{" "}
                </h5>
                <Input
                  className="mb-3"
                  type="text"
                  name="endTime"
                  id="endTime"
                  onChange={(e) => setEndTime(e.target.value)}
                  style={{ textAlign: "center" }}
                  placeholder={bookEnd}
                />
                {/* {date}T{endTime}:00Z */}

                <Button
                  // onClick={toggleNested}
                  type="submit"
                  className="col-12 btn-book"
                >
                  Book
                </Button>
              </FormGroup>
            </Form>
          </Form>

          {/*Modal Kedua*/}
          <Form>
            <div>
              {/* <Button onClick={toggleNested} className="col-12 btn-book">
                Book
              </Button> */}

              <Modal
                isOpen={modalAfterBooking}
                toggle={toggleNested}
                onClosed={closeAll ? toggle : undefined}
                className={className}
                id="modal-after-booking"
              >
                <ModalBody>
                  <h3 className="title">Success, you booked</h3>
                  <br />
                  <h2 style={{ textAlign: "center" }}>{props.name}</h2>
                  <br />
                  <h4 style={{ textAlign: "center", color: "#53c9c2" }}>
                    Ticket ID
                  </h4>
                  <br />
                  <br />
                  <h1 style={{ textAlign: "center" }}>1243243</h1>
                  <br />
                  <br />
                  <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                    Date : {startTime}
                  </h5>
                  <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                    Time : {endTime}
                  </h5>
                  <br />
                  <Button className="col-12 btn-primary" onClick={toggleAll}>
                    OK
                  </Button>
                </ModalBody>
              </Modal>
            </div>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalBook;
