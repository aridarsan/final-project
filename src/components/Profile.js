//package
import {
  Container,
  Button,
  Row,
  Col,
  Modal,
  ModalBody,
  Card,
  CardBody,
} from "reactstrap";
import React, { useEffect, useState } from "react";
import { Rating } from "@material-ui/lab";
import axios from "axios";
import moment from "moment";
import EditProfile from "./EditProfile";
import avatardef from "../icon/avatar2.png";
import Loading from "./Loading";
//modal
import Feedback from "./Feedback";
import PaymentConfirmation from "./PaymentConfirmation";
import ViewTicket from "./ViewTicket";
//styling
import "@fortawesome/fontawesome-free";
import "../styles/Profile.css";

const Profile = () => {
  //Modal
  const [modalEditProfile, setModalEditProfile] = useState(false);
  const [modalFeedback, setModalFeedback] = useState(false);
  const [modalPayment, setModalPayment] = useState(false);
  const [modalTicket, setModalTicket] = useState(false);
  //Data
  const [userData, setUserData] = useState([]);
  const [userBooking, setUserBooking] = useState("");
  const [userReview, setUserReview] = useState("");
  //toglle
  const toggleEditProfile = () => setModalEditProfile(!modalEditProfile);
  const toggleFeedback = () => setModalFeedback(!modalFeedback);
  const togglePayment = () => setModalPayment(!modalPayment);
  const toggleTicket = () => setModalTicket(!modalTicket);

  const baseUrl = "http://kickin.southeastasia.cloudapp.azure.com";
  const token = localStorage.getItem("token");
  const id = localStorage.getItem("id");

  //authorization
  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  useEffect(() => {
    getUserProfile();
    getBookHistory();
    getUserReview();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //Get User Profile
  const getUserProfile = () => {
    axios
      .get(`${baseUrl}/profile?id=${id}`, config)
      .then((res) => {
        // console.log(res.data);
        setUserData(res.data);
      })
      .catch((err) => console.log(err));
  };

  //Book History
  const getBookHistory = () => {
    axios
      .get(`${baseUrl}/booking/user/list?id=${id}`, config)
      .then((res) => {
        setUserBooking(res.data.data.getUserBooking);
        console.log(res.data.data.getUserBooking);
      })
      .catch((err) => console.log(err));
  };

  //Get User Review
  const getUserReview = () => {
    axios
      .get(`${baseUrl}/review/read/${id}`, config)
      .then((res) => {
        setUserReview(res.data);
        // console.log(userReview);
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <div>
        <Container className="Profile">
          <Row>
            {/* User Profil */}
            <Col md={4}>
              {userData ? (
                <div>
                  {userData.map((user) => (
                    <Col
                      style={{
                        border: "1px solid #7b7b7b",
                        borderRadius: "10px",
                      }}
                    >
                      <Col style={{ margin: "20px auto" }}>
                        <h3 className="bio" style={{ textAlign: "center" }}>
                          User Profil
                        </h3>
                        <br />
                        <img
                          className="profil-picture"
                          src={user.photo === null ? avatardef : user.photo}
                          alt="logo"
                        />
                        <h3 className="fullname">{user.fullname}</h3>
                        <h5>Email : {user.email}</h5>
                        <h5>
                          Phone :{" "}
                          {user.phoneNumber === null ? "-" : user.phoneNumber}
                        </h5>
                        <h4 className="bio">Bio</h4>
                        <p>{user.bio}</p>
                        <br />
                        <Button
                          onClick={toggleEditProfile}
                          className="button-profil btn-primary"
                        >
                          Edit Profil
                        </Button>
                        <Modal
                          isOpen={modalEditProfile}
                          toggle={toggleEditProfile}
                          className="custom-modal-size"
                        >
                          <ModalBody>
                            <EditProfile />
                          </ModalBody>
                        </Modal>
                      </Col>
                    </Col>
                  ))}
                </div>
              ) : (
                <Loading />
              )}
              <br />
              {/* User Review */}
              {userReview ? (
                <Col
                  style={{
                    border: "1px solid #7b7b7b",
                    borderRadius: "10px",
                  }}
                >
                  <h3 className="mt-3 ml-3">My Review</h3>
                  <Col style={{ margin: "20px auto" }}>
                    {userReview.map((review) => (
                      <div>
                        <Card
                          className="col-sm-12 mb-3 mt-3"
                          style={{
                            backgroundColor: "#313131",
                            border: "none",
                          }}
                        >
                          <CardBody>
                            <h4 className="review-title">
                              <b>{review.field.name}</b>
                            </h4>
                            <Rating
                              name="half-rating-read"
                              defaultValue={review.rating}
                              precision={0.2}
                              max={5}
                              readOnly
                            />
                            {/* <Rating name="half-rating-read" defaultValue={data.rating} precision={0.5} max={10} readOnly /><p><b>{data.rating} / 10</b></p> */}

                            <p className="review-p">{review.comment}</p>
                          </CardBody>
                        </Card>
                      </div>
                    ))}
                  </Col>
                </Col>
              ) : (
                <Loading />
              )}
            </Col>

            {/* User Booking */}
            <Col>
              <div className="book-history">
                <h3 className="h4-book-history">Book History</h3>
                {userBooking ? (
                  <div>
                    {userBooking.map((booking) => (
                      <div>
                        <div className="history">
                          <br />
                          {booking.isSuccessful === false ? (
                            <h4 className="comingupmatch">Coming up Match</h4>
                          ) : (
                            <h4 className="comingupmatch">Completed Match</h4>
                          )}
                          <Row className="fielddate mt-3">
                            <Col className="fielddate">
                              <h4 className="fieldname">
                                {booking.fieldTime.field.name}
                              </h4>
                            </Col>
                            <Col className="fielddate">
                              <h4 className="fieldname">
                                {moment(booking.startTime).format(
                                  "D MMM YYYY, k:mm "
                                )}
                                {/* {booking.startTime.split("T")[0]} */}
                              </h4>
                            </Col>
                          </Row>
                          <a
                            href={booking.fieldTime.field.addressUrl}
                            target="blank_"
                          >
                            <Button className="direction">
                              <i class="fas fa-location-arrow mr-2"></i>
                              Direction
                            </Button>
                          </a>
                          <br />
                          {booking.transactions[0].payments[0].status !==
                          "Unpaid" ? (
                          <>
                            <Button
                              disabled
                              color="success"
                              className="direction mt-3 mb-3"
                            >
                              <i class="fas fa-check mr-2"></i>
                              Waiting for Confirmation
                            </Button>
                          </>
                          ) : (
                          <>
                            <Button
                              onClick={togglePayment}
                              color="danger"
                              className="direction mt-3 mb-3"
                            >
                              <i class="fas fa-check mr-2"></i>
                              Confirmation Now
                            </Button>
                            <Modal
                              isOpen={modalPayment}
                              toggle={togglePayment}
                              className="custom-modal-size"
                            >
                              <PaymentConfirmation />
                            </Modal>
                          </>
                          )}
                          
                          {/* <Button
                            onClick={togglePayment}
                            color="danger"
                            className="direction mt-3 mb-3"
                          >
                            <i class="fas fa-check mr-2"></i>
                            Confirmation Now
                          </Button>
                          <Modal
                            isOpen={modalPayment}
                            toggle={togglePayment}
                            className="custom-modal-size"
                          >
                            <PaymentConfirmation />
                          </Modal> */}
                        </div>

                        <br />
                      </div>
                    ))}
                  </div>
                ) : (
                  <Loading />
                )}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Profile;
