import React, { useEffect, useState } from "react";
import "@fortawesome/fontawesome-free";
import {
  Row,
  Col,
  Jumbotron,
  Container,
  Button,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";
import "../styles/Home.css";
import header from "../img/header-bg.png";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import Loading from "./Loading";

const Home = (props) => {
  const [fields, setFields] = useState([]);
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const url = "http://kickin.southeastasia.cloudapp.azure.com";
  // const url = "https://kickin-app.herokuapp.com"

  useEffect(() => {
    axios
      .get(url + "/fields/read/all")
      .then((res) => {
        setFields(res.data);
        setLoading(true);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      <div className="text-center">
        <Jumbotron
          style={{
            backgroundImage: `url(${header})`,
            backgroundSize: "cover",
            height: "320px",
          }}
        >
          <Container className="text-center">
            <h2 style={{ color: "#e5e5e5", letterSpacing: "0.2em" }}>
              Let's Play Futsal
            </h2>
            <Link to="/browse-field">
              <Button className="btn-primary-lg">
                <h4
                  style={{
                    marginBottom: "0",
                    padding: "10px",
                    color: "#313131",
                  }}
                >
                  Book Now
                </h4>
              </Button>
            </Link>
          </Container>
        </Jumbotron>
      </div>

      <div>
        <Container>
          <Row style={{ marginRight: "0", marginLeft: "0" }}>
            <h4 className="text-white"> Featured Field</h4>{" "}
            <Link to="/browse-field" className="ml-auto">
              <h4 className="browseField">Browse Field &#10140;</h4>
            </Link>
          </Row>

          <Row className="text-white mt-3">
            {fields.length !== 0 && loading ? (
              fields.slice(1, 5).map((field) => (
                <Col md={3} className="class-col">
                  <Card className="card-field">
                    {field.fieldPhotos.slice(0, 1).map((photo) => (
                      <CardImg
                        className="card-img"
                        top
                        width="100%"
                        src={photo.photoUrl}
                        alt="field photo"
                      />
                    ))}

                    <CardBody className="card-body">
                        <CardTitle tag="h4">{field.name}</CardTitle>
                        <CardSubtitle tag="h5" className="mt-0 ml-auto">
                          Rp. {field.price}
                        <i class="fas fa-tag ml-2"></i>
                        </CardSubtitle>
                      <CardText className="mt-2"><i class="fas fa-map-marker-alt"></i>{field.address}</CardText>
                      <Row style={{ padding: "0 16px" }}>
                        <Link className="col-5 view">
                          <Button
                            block
                            className="btn-second"
                            onClick={() => {
                              history.push(`/field-detail/${field.id}`);
                            }}
                          >
                            View
                          </Button>
                        </Link>

                        <Link className="col-5 view ml-auto">
                          <Button
                          block
                            className="btn-primary"
                            onClick={() => {
                              history.push(`/booking-page/${field.id}`);
                            }}
                          >
                            Book
                          </Button>
                        </Link>

                        {/* <div className="ml-auto">
                          <ModalBookHome id={field.id} name={field.name}/>
                          </div> */}
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              ))
            ) : (
              <Loading />
            )}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Home;
