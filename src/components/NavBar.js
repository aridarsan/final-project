import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import NavBarDropdown from "./NavBarDropdown";
// import icon from "../icon/icon.png";
import {
  Container,
  Collapse,
  Navbar,
  NavItem,
  NavbarBrand,
  NavbarToggler,
  Nav,
} from "reactstrap";

const NavBarPostLogin = (props) => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  const token = localStorage.getItem("token");
  if (token !== null) {props.setIsLogin(true)}
  else { props.setIsLogin(false)}

  console.log(props.isLogin);

  return (
    <Navbar className="fixed-top" style={{ backgroundColor: "#222222" }} expand="md">
      <Container>
        <NavLink to="/">
          <NavbarBrand>
            <h3 className="mt-2 logo">
              <em>KICKIN</em>
            </h3>
          </NavbarBrand>
        </NavLink>

        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav>
            <NavItem>
              <NavLink to="/" activeClassName="activeNav" className="nav-link">
                <h5 className="mt-2 navbar-text">Home</h5>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink to="/" activeClassName="activeNav" className="nav-link">
                <h5 className="mt-2 navbar-text">About</h5>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink to="/browse-field" activeClassName="activeNav" className="nav-link">
                <h5 className="mt-2 navbar-text">Browse</h5>
              </NavLink>
            </NavItem>
          </Nav>

          <Nav className="mt-2 mb-2 ml-auto ">
            {props.isLogin === false && (
              <>
                <NavItem className="mr-4">
                  <Register setIsLogin={props.setIsLogin} />
                </NavItem>

                <NavItem>
                  <Login setIsLogin={props.setIsLogin} />
                </NavItem>
              </>
            )}

            {props.isLogin === true && (
              <>
                <NavItem>
                  <NavBarDropdown isLogin={props.isLogin} setIsLogin={props.setIsLogin} />
                </NavItem>
              </>
            )}
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBarPostLogin;
