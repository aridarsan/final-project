import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  Row,
  Col,
  Container,
  ButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
} from "reactstrap";
import swal from "sweetalert";
import classnames from "classnames";
import rentalshoe from "../img/shoe.png";
import rentalsocks from "../img/socks.png";
import rentalvest from "../img/vests.png";
import { Redirect } from "react-router-dom";


const BookingPage = (props) => {
  const [activeTab, setActiveTab] = useState("1");

  const toggletab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const [dropdownOpenSocks, setOpenSocks] = useState(false);
  const [dropdownOpenShoe, setOpenShoe] = useState(false);
  const [dropdownOpenVest, setOpenVest] = useState(false);

  const toggledropSocks = () => setOpenSocks(!dropdownOpenSocks);
  const toggledropShoe = () => setOpenShoe(!dropdownOpenShoe);
  const toggledropVest = () => setOpenVest(!dropdownOpenVest);

  if (props.isLogin !== true){
    swal({
      icon: "warning",
      title: "Hello Guest",
      text: "please login first to booking a field",
      type: "warning",
      buttons: false,
      timer: 3000,
    });
    return <Redirect to="/" />
  }

  return (
    <Container className="booking">
      <Card
        style={{
          textAlign: "center",
          backgroundColor: "#313131",
          height: "100px",
          padding: "10px",
        }}
      >
        <h3 className="m-auto">Booking Alpen Futsal</h3>
      </Card>
      <Row className="mt-3">
        <Container>
          <h3>Select Date and Time</h3>
        </Container>
      </Row>
      <Nav tabs className="mt-3">
        <NavItem>
          <NavLink
            className={classnames({ activetab: activeTab === "1" })}
            onClick={() => {
              toggletab("1");
            }}
          >
            21-12-2020
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={classnames({ activetab: activeTab === "2" })}
            onClick={() => {
              toggletab("2");
            }}
          >
            22-12-2020
          </NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="3">
              <Button block color="info">
                12:00-13:00 Available
              </Button>
            </Col>

            <Col sm="3">
              <Button block color="danger">
                13:00-14:00 Already Booked
              </Button>
            </Col>

            <Col sm="3">
              <Button block color="success">
                14:00-15:00 Booked
              </Button>
            </Col>

            <Col sm="3">
              <Button block color="warning">
                15:00-16:00 Close
              </Button>
            </Col>
          </Row>
        </TabPane>

        <TabPane tabId="2">
          <Row>
            <Col sm="3">
              <Button block>12:00-13:00 Available</Button>
            </Col>

            <Col sm="3">
              <Button block>12:00-13:00 Available</Button>
            </Col>

            <Col sm="3">
              <Button block>12:00-13:00 Available</Button>
            </Col>

            <Col sm="3">
              <Button block>12:00-13:00 Available</Button>
            </Col>
          </Row>
        </TabPane>
      </TabContent>
      <br />

      <h3>Additional Rental Kit</h3>
      <Container>
        <Row className="mt-4" style={{ borderRadius: "5px" }}>
          <div
            className="col-4"
            style={{ backgroundColor: "#313131", padding: "10px" }}
          >
            <Row>
              <Col sm="3">
                <img src={rentalsocks} alt="" />
              </Col>

              <Col sm="5" style={{ padding: "0px 15px" }}>
                <Row>
                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <h4>Socks</h4>
                    <p>Rp. 5.000</p>
                  </Col>

                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <ButtonDropdown
                      isOpen={dropdownOpenSocks}
                      toggle={toggledropSocks}
                    >
                      <DropdownToggle caret>size</DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem>XXL</DropdownItem>
                        <DropdownItem>XL</DropdownItem>
                        <DropdownItem>L</DropdownItem>
                        <DropdownItem>M</DropdownItem>
                        <DropdownItem>S</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                  </Col>
                </Row>
              </Col>
              <Row>
                <Col sm="4" className="m-auto">
                  <h4>qty</h4>
                </Col>
                <Col className="m-auto">
                  {" "}
                  <input
                    placeholder="1"
                    min={0}
                    max={10}
                    type="number"
                    step="1"
                    style={{ width: "50px", height: "20px" }}
                    className="input-qty"
                  />
                </Col>
              </Row>
            </Row>
          </div>

          <div
            className="col-4"
            style={{ backgroundColor: "#313131", padding: "10px" }}
          >
            <Row>
              <Col sm="3">
                <img src={rentalvest} alt="" />
              </Col>

              <Col sm="5" style={{ padding: "0px 15px" }}>
                <Row>
                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <h4>Vest</h4>
                    <p>Rp. 10.000</p>
                  </Col>

                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <ButtonDropdown
                      isOpen={dropdownOpenVest}
                      toggle={toggledropVest}
                    >
                      <DropdownToggle caret>size</DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem>XXL</DropdownItem>
                        <DropdownItem>XL</DropdownItem>
                        <DropdownItem>L</DropdownItem>
                        <DropdownItem>M</DropdownItem>
                        <DropdownItem>S</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                  </Col>
                </Row>
              </Col>
              <Row>
                <Col sm="4" className="m-auto">
                  <h4>qty</h4>
                </Col>
                <Col className="m-auto">
                  {" "}
                  <input
                    placeholder="1"
                    min={0}
                    max={10}
                    type="number"
                    step="1"
                    style={{ width: "50px", height: "20px" }}
                    className="input-qty"
                  />
                </Col>
              </Row>
            </Row>
          </div>

          <div
            className="col-4"
            style={{ backgroundColor: "#313131", padding: "10px" }}
          >
            <Row>
              <Col sm="3">
                <img src={rentalshoe} alt="" />
              </Col>

              <Col sm="5" style={{ padding: "0px 15px" }}>
                <Row>
                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <h4>Shoe</h4>
                    <p>Rp. 15.000</p>
                  </Col>

                  <Col sm="6" className="m-auto" style={{ padding: "0px" }}>
                    <ButtonDropdown
                      isOpen={dropdownOpenShoe}
                      toggle={toggledropShoe}
                    >
                      <DropdownToggle caret>size</DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem>40</DropdownItem>
                        <DropdownItem>41</DropdownItem>
                        <DropdownItem>42</DropdownItem>
                        <DropdownItem>43</DropdownItem>
                        <DropdownItem>44</DropdownItem>
                        <DropdownItem>45</DropdownItem>
                      </DropdownMenu>
                    </ButtonDropdown>
                  </Col>
                </Row>
              </Col>
              <Row>
                <Col sm="4" className="m-auto">
                  <h4>qty</h4>
                </Col>
                <Col className="m-auto">
                  {" "}
                  <input
                    placeholder="1"
                    min={0}
                    max={10}
                    type="number"
                    step="1"
                    style={{ width: "50px", height: "20px" }}
                    className="input-qty"
                  />
                </Col>
              </Row>
            </Row>
          </div>
        </Row>
      </Container>

      <Container>
        <Row>
          <Col sm="7" className="mt-4">
            {/* Row Pertama tidak mempunyai background*/}
            <Row className="mb-3">
              <h3>Review Your Book</h3>
            </Row>
            {/* Row Kedua */}
            <Row
              style={{
                backgroundColor: "#313131",
                border: "1px solid #53c9c2",
                borderBottom: "none",
              }}
            >
              <Col sm="6" className="mt-4 1">
                <Row>
                  <Col>
                    <h4>Field Name</h4>
                  </Col>

                  <Col>
                    <h5>Alpen Futsal</h5>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <h4>Adress</h4>
                  </Col>

                  <Col>
                    <h5>Jl. jali-jali gang macan No.40</h5>
                  </Col>
                </Row>
              </Col>

              <Col sm="6" className="mt-4 2">
                <Row>
                  <Col>
                    <h4>Date</h4>
                  </Col>

                  <Col>
                    <h5>21-12-2020</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>Time</h4>
                  </Col>

                  <Col>
                    <h5>20:00-22:00</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>Duration</h4>
                  </Col>

                  <Col>
                    <h5>2 Hours</h5>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row
              style={{
                backgroundColor: "#313131",
                border: "1px solid #53c9c2",
                borderTop: "none",
                borderBottom: "none",
              }}
            >
              <Col style={{ paddingLeft: "15px" }}>
                <h4>Rental Kit</h4>
              </Col>
            </Row>

            <Row
              style={{
                backgroundColor: "#313131",
                border: "1px solid #53c9c2",
                borderTop: "none",
              }}
            >
              <Col sm="4" className="mb-4 1">
                <Row>
                  <Col className="col-5">
                    <img src={rentalsocks} alt="" height="50px" />
                  </Col>

                  <Col style={{ padding: "0px" }}>
                    <h5>Rp. 5000 </h5>
                    <h5>X 1 = Rp. 5000</h5>
                  </Col>
                </Row>
              </Col>

              <Col sm="4" className="mb-4 1">
                <Row>
                  <Col className="col-5">
                    <img src={rentalvest} alt="" height="50px" />
                  </Col>

                  <Col style={{ padding: "0px" }}>
                    <h5>Rp. 10000 </h5>
                    <h5>X 1 = Rp. 10000</h5>
                  </Col>
                </Row>
              </Col>

              <Col sm="4" className="mb-4 1">
                <Row>
                  <Col className="col-5">
                    <img src={rentalshoe} alt="" height="50px" />
                  </Col>

                  <Col style={{ padding: "0px" }}>
                    <h5>Rp. 15000 </h5>
                    <h5>X 1 = Rp. 15000</h5>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="mt-3">
              <Col>
                <h4>Description</h4>
              </Col>
            </Row>
            <Row>
              <Col>
                <p>
                  If you click make payment it means you agree with the terms
                  and will pay according to the total to be paid. If you dont
                  pay within 24 hours, the field order will be canceled.
                </p>
              </Col>
            </Row>
          </Col>

          <Col sm="5" className="mt-4 mb-4">
            <Row className="mb-3">
              <h3>Total Payment</h3>
            </Row>

            <Row
              style={{
                backgroundColor: "#313131",
                border: "1px solid #53c9c2",
              }}
            >
              <Col className="mt-4">
                <Row>
                  <Col>
                    <h4>Field</h4>
                  </Col>
                  <Col sm="8">
                    <h5>Rp. 240.000</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>Rental Kit</h4>
                  </Col>
                  <Col sm="8">
                    <h5>Rp. 30.000</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4 style={{ color: "#53c9c2" }}>Total</h4>
                  </Col>
                  <Col sm="8">
                    <h5 style={{ color: "#53c9c2" }}>Rp. 270.000</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>TRANSFER PAYMENT TO</h4>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>Name</h4>
                  </Col>
                  <Col sm="8">
                    <h5>PT. KICKIN MAJU</h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4>No. Account</h4>
                  </Col>
                  <Col sm="8" className="mb-4">
                    <h5>343535345436 (BANK BCA)</h5>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mb-3 mt-5">
              <Button block className="btn-primary">
                Make Payment
              </Button>
            </Row>
          </Col>
        </Row>
      </Container>
    </Container>
  );
};

export default BookingPage;
