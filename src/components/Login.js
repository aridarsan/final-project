/* eslint-disable no-useless-escape */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Spinner,
  Alert,
  Row,
} from "reactstrap";
import Register from "./Register";
import { useGoogleLogin } from "react-google-login";

const Login = (props) => {
  const clientId =
    "257695633255-8ut398jnnq4f4s5pj99irv46mishs6i2.apps.googleusercontent.com";
  //Define Function
  const urlLogin = "http://kickin.southeastasia.cloudapp.azure.com/auth/login";
  const urlGoogleLogin = "http://kickin.southeastasia.cloudapp.azure.com/auth/login/google";
  // const urlLogin = "https://kickin-app.herokuapp.com/auth/login";

  const { className } = props;

  const history = useHistory();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [googleToken, setGoogleToken] = useState()

  const checkerLogin = () => {
    //Password and Email Formatting
    let mailformat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    let passwordformat = /^(?=.*[0-9])(?=.*[A-Z]).{8,32}$/g;

    if (!email) {
      setMessage("Email Must be Filled!");
      return false;
    } else if (!email.match(mailformat)) {
      setMessage("Email Invalid!");
      return false;
    } else if (!password) {
      setMessage("Password Must be Filledd!");
      return false;
    } else if (!password.match(passwordformat)) {
      setMessage("Password Invalid!");
      return false;
    } else {
      setIsSubmitting(true);
    }
  };

  //Submit Action
  const handleSubmit = (e) => {
    e.preventDefault();
    if (checkerLogin() !== false) {
      const data = {
        email: email,
        password: password,
      };

      //Post Data
      axios
        .post(urlLogin, data)
        .then((res) => {
          const { id, token } = res.data;
          localStorage.setItem("id", id);
          localStorage.setItem("token", token);
          setModalLogin(false);
          console.log(props);
          props.setIsLogin(true);
          history.push("/");
          swal({
            icon: "success",
            title: "Success Login",
            text: "let's book a field",
            type: "success",
            buttons: false,
            timer: 3000,
          });
        })
        .catch((err) => {
          console.log(err);
          setIsSubmitting(false);
          swal({
            icon: "error",
            title: "Wrong email or password",
            text: "please try again",
            type: "warning",
            buttons: false,
            timer: 2000,
          });
        });
    }
  };

  const handleSubmitGoogle = (e) => {
    e.preventDefault()
      const data = {
        token : googleToken
      };
      //Post Data
      axios
        .post(urlGoogleLogin, data)
        .then((res) => {
          const { id, token } = res.data;
          localStorage.setItem("id", id);
          localStorage.setItem("token", token);
          setModalLogin(false);
          console.log(props);
          props.setIsLogin(true);
          history.push("/");
          swal({
            icon: "success",
            title: "Success Login",
            text: "let's book a field",
            type: "success",
            buttons: false,
            timer: 3000,
          });
        })
        .catch((err) => {
          console.log(err);
          setIsSubmitting(false);
          swal({
            icon: "error",
            title: "Wrong email or password",
            text: "please try again",
            type: "warning",
            buttons: false,
            timer: 2000,
          });
        });
  }

  const [modalLogin, setModalLogin] = useState(false);

  const toggleLogin = () => setModalLogin(!modalLogin);

  //google auth
  const onSuccess = (res) => {
    console.log("Login Success: currentUser:", res.profileObj);
    setGoogleToken(res.tokenId)
    // handleSubmitGoogle();
    console.log(res.tokenId)
    // alert(
    //   `Logged in successfully welcome ${res.profileObj.name} 😍.`
    // );
  };

  const onFailure = (res) => {
    console.log("Login failed: res:", res);
    // alert(`Failed to login`);
  };

  const { signIn } = useGoogleLogin({
    onSuccess,
    onFailure,
    clientId,
    isSignedIn: true,
    accessType: "offline",
    // responseType: 'code',
    // prompt: 'consent',
  });

  const submit = () => {
    signIn();
    if ( googleToken != null){
      handleSubmitGoogle();
    }
  }

  console.log(googleToken)

  return (
    <div>
      <Link
        onClick={toggleLogin}
        style={{
          textDecoration: "none",
        }}
      >
        <h5 className="mr-4 mb-sm-0 btn-login">Log In</h5>
      </Link>
      <Modal
        isOpen={modalLogin}
        toggle={toggleLogin}
        className={className}
        id="modal-login"
      >
        <ModalBody>
          <h3 className="title">Log In</h3>
          <FormGroup>
            {message && <Alert color="danger">{message}</Alert>}
          </FormGroup>

          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                className="col-12 m-auto"
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                className="col-12 m-auto"
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              {isSubmitting ? (
                <Button className="col-12 btn-primary" type="submit" disabled>
                  <Spinner color="light" />
                </Button>
              ) : (
                <Button className="col-12 btn-primary" type="submit">
                  <i class="fas fa-sign-in-alt mr-2"></i>
                  <strong>Log In</strong>
                </Button>
              )}
            </FormGroup>
            <p className="or">Or</p>

            <FormGroup>
              <Button onClick={() =>submit()} className="col-12 btn-facebook">
                <i class="fab fa-google mr-2"></i>
                Log In with Google
              </Button>
            </FormGroup>

            <Row>
              <p className="text-bottom">
                Don't have an account?{" "}
                {/* <Link to="/register">
                <strong>Sign Up</strong>
              </Link> */}
              </p>
              <Register />
            </Row>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default Login;
