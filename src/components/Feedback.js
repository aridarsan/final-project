import React from "react";
import { Button, Input, Label } from "reactstrap";
import ReactStars from "react-rating-stars-component";
import "../styles/Feedback.css";

const Feedback = (props) => {
  const ratingChanged = (newRating) => {
    console.log(newRating);
  };

  return (
    <>
        <div className="modal-content">
          <br />
          <h3 className="modal-title" style={{ textAlign: "center" }}>
            Leave your Feedback
          </h3>
          <div className="modal-body">
            <form>
              <div className="form-group">
                <Label className="label">Select Rating</Label>
                <ReactStars
                  count={5}
                  onChange={ratingChanged}
                  size={50}
                  activeColor="#FFCB74"
                />
                ,
              </div>
              <div className="form-group">
                <Label className="label">Your Feedback</Label>
                <Input type="textarea" className="form-control" rows="4" />
              </div>
              <br />
              <div className="form-group">
                <Button
                  type="submit"
                  className="btn btn-primary btn-block btn-lg"
                  value="Submit"
                >
                  Submit
                </Button>
              </div>
            </form>
          </div>
        </div>
    </>
  );
};

export default Feedback;
