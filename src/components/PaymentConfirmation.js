import React, { useRef} from "react";
import {Row, Col, Table, Button } from "reactstrap";
import "../styles/PaymentConfirmation.css";
import receipt from "../img/shoe.png"

const PaymentConfirmation = () => {
  const inputFile = useRef(null);
  const onTextClick = () => {
    inputFile.current.click();
  };

  return (
    <>
      <div className="modal-dialog">
        <div className="modal-content">
          <h3 className="modal-title">Payment Confirmation</h3>
          <div className="modal-body">
            <h4>TRANSFER TO</h4>
            <Table className="table-borderless">
              <tr>
                <th scope="row">Name</th>
                <td>PT. KICKIN MAJU</td>
              </tr>
              <tr>
                <th scope="row">No. Account</th>
                <td>5255338878 (Bank BCA)</td>
              </tr>
              <tr>
                <th scope="row" className="total">
                  Total
                </th>
                <td className="total">Rp xxx.xxx</td>
              </tr>
            </Table>
            <h4>UPLOAD RECEIPT</h4>
            <br />
            <form method="post">
              <Row>
                <Col sm="6">
                <img src={receipt} alt="" width="100%"/>
                </Col>
                <Col style={{textAlign:"center"}}>
                <input type="file" id="myfile" ref={inputFile} />
                <Button block onClick={onTextClick} style={{textAlign:"center"}}>
                  Select File
                </Button>
                </Col>
              </Row>
              <br />
              <p>
                After upload your receipt and click submit, we will check the
                receipt if you our admin confirm your receipt. You wil get your
                ticket on Profile
              </p>
              <Button
                block
                type="submit"
                className="btn btn-primary"
                value="Submit"
              >
                Submit
              </Button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default PaymentConfirmation;
