import React, { useEffect, useState } from "react";
import axios from "axios";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const ModalBook = (props) => {
  const [modalBook, setModalBook] = useState(false);
  const [modalAfterBooking, setModalAfterBooking] = useState(false);
  const [closeAll, setCloseAll] = useState(false);
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [bookingInput, setBookingInput] = useState(null)
  const { className } = props;
  const toggle = () => setModalBook(!modalBook);

  const toggleNested = () => {
    setModalAfterBooking(!modalAfterBooking);
    setCloseAll(false);
  };
  
  const toggleAll = () => {
    setModalAfterBooking(!modalAfterBooking);
    setCloseAll(true);
  };

  const baseUrl = "http://kickin.southeastasia.cloudapp.azure.com";

  // const token = localStorage.getItem("token");
  // const id = localStorage.getItem("id");

  useEffect(() => {
    // var config = {
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: "Bearer " + token,
    //   },
    // };
    console.log(props.id)

    axios
      .get(`${baseUrl}/booking?id=${props.id}`)
      .then((res) => {
        console.log(res.data);
        setBookingInput(res.data);
      })
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  console.log(bookingInput)

  const handleBook = (e) => {
    e.preventDefault();
    // const data = {
    //   date:date,
    //   time:time
    // }

    axios
    .post()
    .then((res) => {
      // setModalBook(false);
      console.log(props);
    });
  };

  return (
    //Modal Utama
    <div>
      <Button onClick={toggle} className="col btn-book">
      <i class="fas fa-bookmark mr-2"></i>
        Book Now
      </Button>

      <Modal
        isOpen={modalBook}
        toggle={toggle}
        className={className}
        id="modal-book"
      >
        <ModalBody>
          <h3 className="title">Field A</h3>
          <Form onSubmit={handleBook}>
            <FormGroup>
              <Label for="date">Date</Label>
              <Input
                type="date"
                name="date"
                id="date"
                className="col-12 m-auto"
                onChange={(e) => setDate(e.target.value)}
              />
            </FormGroup>
            
            <FormGroup>
              <Label for="Time">Time</Label>
              <Input
                type="Time"
                name="Time"
                id="Time"
                className="col-12 m-auto"
                onChange={(e) => setTime(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <h4>Review Your Book</h4>
              <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                Date : {date}
              </h5>
              <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                Time : {time}
              </h5>
            </FormGroup>

            {/*Modal Kedua*/}
            <div>
              <Button onClick={toggleNested} className="col-12 btn-book">
                Book
              </Button>

              <Modal
                isOpen={modalAfterBooking}
                toggle={toggleNested}
                onClosed={closeAll ? toggle : undefined}
                className={className}
                id="modal-after-booking"
              >
                <ModalBody>
                  <h3 className="title">Success, you booked</h3>
                  <br />
                  <h2 style={{ textAlign: "center" }}>Field A</h2>
                  <br />
                  <h4 style={{ textAlign: "center", color: "#53c9c2" }}>
                    Ticket ID
                  </h4>
                  <br />
                  <br />
                  <h1 style={{ textAlign: "center" }}>1243243</h1>
                  <br />
                  <br />
                  <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                    Date : {date}
                  </h5>
                  <h5 style={{ textAlign: "center", fontWeight: "700" }}>
                    Time : {time}
                  </h5>
                  <br />
                  <Button className="col-12 btn-primary" onClick={toggleAll}>
                    OK
                  </Button>
                </ModalBody>
              </Modal>
            </div>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalBook;
