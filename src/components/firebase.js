import * as React from "react";
// import { render } from "react-dom";
import {
  FirebaseAuthProvider,
  FirebaseAuthConsumer,
} from "@react-firebase/auth";
import firebase from "firebase/app";
import "firebase/auth";

// import { config } from "./test-credentials";

const config = {
  apiKey: "AIzaSyCjgUiPFkj7sag4mmHkukEx9WMpnMZI1As",
  authDomain: "kickin-4e530.firebaseapp.com",
  databaseURL: "https://kickin-4e530.firebaseio.com",
  projectId: "kickin-4e530",
  storageBucket: "kickin-4e530.appspot.com",
  messagingSenderId: "257695633255",
  appId: "1:257695633255:web:fe4cbd1e261a37a614e9c2",
};

const IDontCareAboutFirebaseAuth = () => {
  return <div>This part won't react to firebase auth changes</div>;
};

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const submit = () => {
  firebase
    .auth()
    .getRedirectResult(googleAuthProvider)
    .then(function (result) {
      if (result.credential) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        console.log(token);
      }
      // The signed-in user info.
      // var user = result.user;
    })
    .catch(function (error) {
      // // Handle Errors here.
      // var errorCode = error.code;
      // var errorMessage = error.message;
      // // The email of the user's account used.
      // var email = error.email;
      // // The firebase.auth.AuthCredential type that was used.
      // var credential = error.credential;
      // // ...
    });
};

const FireBase = () => {
  return (
    <div className="mt-5">
      <IDontCareAboutFirebaseAuth />
      <FirebaseAuthProvider {...config} firebase={firebase}>
        <div>
          Hello <div>From Auth Provider Child 1</div>
          <FirebaseAuthConsumer>
            {({ isSignedIn, firebase }) => {
              if (isSignedIn === true) {
                return (
                  <div>
                    <h2>You're signed in 🎉 </h2>
                    <button
                      onClick={() => {
                        firebase.app().auth().signOut();
                      }}
                    >
                      Sign out
                    </button>
                  </div>
                );
              } else {
                return (
                  <div>
                    <h2>You're not signed in </h2>
                    <button
                      onClick={() => {
                        firebase.app().auth().signInAnonymously();
                      }}
                    >
                      Sign in anonymously
                    </button>
                    <button onClick={() => submit()}>
                      Sign in with Google
                    </button>
                  </div>
                );
              }
            }}
          </FirebaseAuthConsumer>
        </div>
        <div>Another div</div>
      </FirebaseAuthProvider>
    </div>
  );
};

export default FireBase;
