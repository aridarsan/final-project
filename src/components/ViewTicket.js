import React, { useState } from "react";
import { Table, Button } from "reactstrap";
import "../styles/ViewTicket.css";

const ViewTicket = () => {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  return (
    <>
      <div className="modal-dialog">
        <div className="modal-content">
          <h3 className="modal-title">Ticket</h3>
          <div className="modal-body">
            <text className="ticketcode">XXXXXXXX</text>
            <Table className="table-borderless">
              <tr>
                <th scope="row">Field Name</th>
                <td>Insert field name here</td>
              </tr>
              <tr>
                <th scope="row">Field Address</th>
                <td>Insert field address here</td>
              </tr>
              <tr>
                <th scope="row">Date & Time</th>
                <td>dd-mm-yyyy/hh.mm-hh.mm</td>
              </tr>
              <tr>
                <th scope="row">Duration</th>
                <td>x hours</td>
              </tr>
            </Table>
            <br />
            <p>
            Screenshot this page to be shown at field when will play futsal
            </p>
            <Button className="btn btn-primary" onClick={toggle}>OK</Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewTicket;
