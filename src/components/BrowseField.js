import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "@fortawesome/fontawesome-free";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Row,
  Col,
  Container,
  FormGroup,
  Input,
  CardSubtitle,
  Form,
} from "reactstrap";
import "../styles/BrowseField.css";
import Loading from "./Loading";

const BrowseField = () => {
  // const [dropdownOpenFilter, setOpenFilter] = useState(false);
  const [dropdownOpenSort, setOpenSort] = useState(false);

  // const toggleFilter = () => setOpenFilter(!dropdownOpenFilter);
  const toggleSort = () => setOpenSort(!dropdownOpenSort);

  const [fieldss, setFieldss] = useState([]);
  const [searchLocation, setSearchLocation] = useState("");
  const [searchName, setSearchName] = useState("");
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const url = "http://kickin.southeastasia.cloudapp.azure.com";
  // const url = "https://kickin-app.herokuapp.com"

  useEffect(() => {
    axios
      .get(url + "/fields/read/all")
      .then((res) => {
        setFieldss(res.data);
        console.log(res);
        setLoading(true);
      })
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fieldSearchLocation = (e) => {
    e.preventDefault();
    axios.get(`${url}/fields/location?q=${searchLocation}`).then((res) => {
      setFieldss(res.data[0].fields);
      console.log(res.data[0]);
      setLoading(true);
    });
  };

  const fieldSearchName = (e) => {
    e.preventDefault();
    axios.get(`${url}/fields/name?q=${searchName}`).then((res) => {
      setFieldss(res.data);
      console.log(res.data);
      setLoading(true);
    });
  };

  const sortByName = (e) => {
    e.preventDefault();
    axios.get(`${url}/fields/sort/name`).then((res) => {
      setFieldss(res.data);
      console.log(res.data);
      setLoading(true);
    });
  };

  // const sortByNameReverse = (e) => {
  //   e.preventDefault()
  //   sortByName.reverse()
  // };

  const sortByPrice = (e) => {
    e.preventDefault();
    axios.get(`${url}/fields/sort/price`).then((res) => {
      setFieldss(res.data);
      console.log(res.data);
      setLoading(true);
    });
  };

  return (
    <>
      <Container className="search">
        <Row>
          <Col xs="4">
            <h3 style={{ padding: "15px 0" }}>Browse Field</h3>
          </Col>
          <Col className="col-2"></Col>

          <Col className="col-2">
            <ButtonDropdown isOpen={dropdownOpenSort} toggle={toggleSort}>
              <DropdownToggle className="sort" caret>
                Sort By
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={sortByName}>Name</DropdownItem>
                <DropdownItem onClick={sortByPrice}>Price</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </Col>

          <Col className="col-2">
            <Form onSubmit={fieldSearchLocation}>
              <FormGroup>
                <Input
                  xs="12"
                  type="text"
                  id="searchLocation"
                  name="searchLocation"
                  onChange={(e) => setSearchLocation(e.target.value)}
                  placeholder="Search location"
                ></Input>
              </FormGroup>
            </Form>
          </Col>

          <Col className="col-2">
            <Form onSubmit={fieldSearchName}>
              <FormGroup>
                <Input
                  xs="12"
                  type="text"
                  id="searchLocation"
                  name="searchLocation"
                  onChange={(e) => setSearchName(e.target.value)}
                  placeholder="Search field"
                ></Input>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Container>

      <div className="fields">
        <Container>
          <Row className="text-white mt-3">
            {fieldss && loading ? (
              fieldss.map((field) => (
                <Col md={3} className="class-col">
                  <Card className="card-field">
                    {field.fieldPhotos.slice(1, 2).map((photo) => (
                      <CardImg
                        className="card-img"
                        top
                        width="100%"
                        src={photo.photoUrl}
                        alt="photo field"
                      />
                    ))}

                    <CardBody className="card-body">
                      <CardTitle tag="h4">{field.name}</CardTitle>
                      <CardSubtitle tag="h5" className="mt-0 ml-auto">
                        Rp. {field.price}
                        <i class="fas fa-tag ml-2"></i>
                      </CardSubtitle>
                      <CardText className="mt-2">
                        <i class="fas fa-map-marker-alt"></i>
                        {field.address}
                      </CardText>
                      <Row style={{ padding: "0 16px" }}>
                        <Link className="col-5 view">
                          <Button
                            block
                            className="btn-second"
                            onClick={() => {
                              history.push(`/field-detail/${field.id}`);
                            }}
                          >
                            View
                          </Button>
                        </Link>

                        <Link className="col-5 view ml-auto">
                          <Button
                            block
                            className="btn-primary"
                            onClick={() => {
                              history.push(`/booking-page/${field.id}`);
                            }}
                          >
                            Book
                          </Button>
                        </Link>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              ))
            ) : (
              <Loading />
            )}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default BrowseField;
