import React, { useState } from "react";
import { Link } from "react-router-dom";
// import axios from "axios";
import "@fortawesome/fontawesome-free";
import "../styles/FieldDetail.css";
import { Button, Modal, ModalBody } from "reactstrap";

const ModalBooking = (props) => {
  const [modalAfterBooking, setModalAfterBooking] = useState(false);
  const { className } = props;
  const toggleAfterBook = () => setModalAfterBooking(!modalAfterBooking);

  return (
    <div>
      <Link
        onClick={toggleAfterBook || props.modal}
        style={{
          textDecoration: "none",
        }}
      >
        <Button className="col-12 btn-book">Book</Button>
      </Link>

      <Modal
        isOpen={modalAfterBooking}
        toggle={toggleAfterBook}
        className={className}
        id="modal-after-booking"
      >
        <ModalBody>
          <h3 className="title">Success, you booked</h3>
          <br />
          <h2 style={{ textAlign: "center" }}>Field A</h2>
          <br />
          <h4 style={{ textAlign: "center", color: "#53c9c2" }}>Ticket ID</h4>
          <br />
          <br />
          <h1 style={{ textAlign: "center" }}>1243243</h1>
          <br />
          <br />
          <h5 style={{ textAlign: "center", fontWeight: "700" }}>
            Date : {props.date}
          </h5>
          <h5 style={{ textAlign: "center", fontWeight: "700" }}>
            Time : {props.time}
          </h5>
          <br />
            <Button className="col-12 btn-primary" onClick={toggleAfterBook}>OK</Button>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalBooking;
