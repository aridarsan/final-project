import { Button, Modal } from "reactstrap";
import React, { useState } from "react";
import Feedback from "./Feedback";
import PaymentConfirmation from "./PaymentConfirmation";
import ViewTicket from "./ViewTicket";

const Buttons = () => {
  const [modalFeedback, setModalFeedback] = useState(false);
  const [modalPayment, setModalPayment] = useState(false);
  const [modalTicket, setModalTicket] = useState(false);
  const toggleFeedback = () => setModalFeedback(!modalFeedback);
  const togglePayment = () => setModalPayment(!modalPayment);
  const toggleTicket = () => setModalTicket(!modalTicket);
  const booking = "";

  return (
    <div>
      {booking.isPaid === true ? (
        booking.isSuccessful === true ? (
          <>
            <Button
              onClick={toggleFeedback}
              color="warning"
              className="mt-3 mb-3 direction "
            >
              Give Feedback
            </Button>
            <Modal
              isOpen={modalFeedback}
              toggle={toggleFeedback}
              className="custom-modal-size"
            >
              <Feedback />
            </Modal>
          </>
        ) : (
          <>
            <Button
              onClick={toggleTicket}
              color="danger"
              className="direction mt-3 mb-3"
            >
              <i class="fas fa-check mr-2"></i>
              View Ticket
            </Button>
            <Modal
              isOpen={modalTicket}
              toggle={toggleTicket}
              className="custom-modal-size"
            >
              <ViewTicket />
            </Modal>
          </>
        )
      ) : booking.transactions[0].payments[0].status !== "Unpaid" ? (
        <>
          <Button disabled color="success" className="direction mt-3 mb-3">
            <i class="fas fa-check mr-2"></i>
            Waiting for Confirmation
          </Button>
        </>
      ) : (
        <>
          <Button
            onClick={togglePayment}
            color="danger"
            className="direction mt-3 mb-3"
          >
            <i class="fas fa-check mr-2"></i>
            Confirmation Now
          </Button>
          <Modal
            isOpen={modalPayment}
            toggle={togglePayment}
            className="custom-modal-size"
          >
            <PaymentConfirmation />
          </Modal>
        </>
      )}
    </div>

    /* (booking.transaction[0].payments[0].status === unpaid)

        <>
          <Button
            onClick={toggleTicket}
            color="danger"
            className="direction mt-3 mb-3"
          >
            <i class="fas fa-check mr-2"></i>
            Confirmation Now
          </Button>
          <Modal
            isOpen={modalTicket}
            toggle={toggleTicket}
            className="custom-modal-size"
          >
            <ViewTicket />
          </Modal>

        </>

        <>
          <Button
            onClick={togglePayment}
            color="danger"
            className="direction mt-3 mb-3"
          >
            <i class="fas fa-check mr-2"></i>
            Confirmation Now
          </Button>
          <Modal
            isOpen={modalPayment}
            toggle={togglePayment}
            className="custom-modal-size"
          >
            <PaymentConfirmation />
          </Modal>
        </> */
  );
};
export default Buttons;
